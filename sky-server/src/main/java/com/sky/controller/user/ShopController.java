package com.sky.controller.user;

import com.sky.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

@RestController("userShopController")
@RequestMapping("/user/shop")
@Slf4j
public class ShopController {
    @Autowired
    private RedisTemplate redisTemplate;
    private String key="SHOP_STATUS";


   @GetMapping("/status")
    public Result<Integer> getStatus(){
       log.info("获取商店营业状态");
       // Get the value of the key from Redis
        Integer status = (Integer) redisTemplate.opsForValue().get(key);
        // Return the value of the key
       log.info("营业状态为：{}",status);
        return Result.success(status);
    }
}
