package com.sky.mapper;

import com.sky.entity.Setmeal;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface SetmealMapper {
    /**
     * 根据分类id查询套餐的数量
     * @param id
     * @return
     */
    @Select("select count(id) from setmeal where category_id=#{id}")
    Integer selectCountByCategoryId(Long id);

    @Select("select * from setmeal where name=#{name}")
    Setmeal getByName(String name);

    void addSetmeal(Setmeal setmeal);

    @Select("select count(id) from setmeal")
    Integer count();

    List<SetmealVO> page(int start, int pageSize, String name, Integer status, Integer categoryId);
    @Update("update setmeal set status=#{status} where id=#{id}")
    void updateStatus(Integer status, Long id);
    @Select("select * from setmeal where id=#{id}")
    Setmeal getById(Long id);

    @Update("update setmeal set name=#{name},price=#{price},category_id=#{categoryId}" +
            ",description=#{description},image=#{image}" +
            ",update_time=#{updateTime},update_user=#{updateUser} " +
            "where id=#{id}")
    void update(Setmeal setmeal);

    List<Setmeal> getByIds(List<Long> ids);

    void deleteByIds(List<Long> ids);

    List<Setmeal> list(Setmeal setmeal);
    @Select("select sd.name, sd.copies, d.image, d.description " +
            "from setmeal_dish sd left join dish d on sd.dish_id = d.id " +
            "where sd.setmeal_id = #{setmealId}")
    List<DishItemVO> getDishItemBySetmealId(Long id);
}
