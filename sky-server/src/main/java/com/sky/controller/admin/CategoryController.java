package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/category")
@Slf4j
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 分页查询菜品分类
     * @param categoryPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> page( CategoryPageQueryDTO categoryPageQueryDTO){
        log.info("分页信息:{}",categoryPageQueryDTO);
        PageResult result=categoryService.pageQuery(categoryPageQueryDTO);
        return Result.success(result);
    }
    /**
     * 添加菜品
     */
    @PostMapping
    public Result add(@RequestBody CategoryDTO categoryDTO){
        log.info("菜品信息:{}",categoryDTO);
        categoryService.add(categoryDTO);
        return Result.success();
    }
    /**
     * 修改菜品状态
     */
    @PostMapping("/status/{status}")
    public Result startOrStop(@PathVariable Integer status,Long id){
        log.info("菜品id:{},菜品状态:{}",id,status);
        categoryService.updateStatusById(id,status);
        return Result.success();
    }

    /**
     * 编辑菜品信息
     * @param categoryDTO
     * @return
     */
    @PutMapping()
    public Result endit(@RequestBody CategoryDTO categoryDTO){
        log.info("修改的对象:{}",categoryDTO);
        categoryService.edit(categoryDTO);
        return Result.success();
    }
    /**
     * 删除菜品信息
     */
    @DeleteMapping()
    public Result delete( Long id){
        log.info("要删除菜品的id:{}",id);
        categoryService.deleteById(id);
        return Result.success();
    }
    /**
     * 获取分类
     */
    @GetMapping("/list")
    public Result<List<Category>> list(Integer type)
    {
        log.info("分类信息:{}",type);
       List<Category> list= categoryService.list(type);
        return Result.success(list);
    }
}
