package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/admin/setmeal")
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    /**
     * 添加套餐信息
     * @param setmealDTO
     * @return
     */
    @PostMapping()
    public Result addSetmeal(@RequestBody  SetmealDTO setmealDTO){
        log.info("添加套餐：{}",setmealDTO);
        setmealService.addSetmeal(setmealDTO);
        return Result.success();
    }
    /**
     * 套餐分页查询
     */
    @GetMapping("/page")
    public Result<PageResult> page(SetmealPageQueryDTO setmealPageQueryDTO){
        log.info("套餐分页查询：{}",setmealPageQueryDTO);
               log.info("套餐分页查询：{}",setmealPageQueryDTO);
        PageResult setmealVOPageResult = setmealService.page(setmealPageQueryDTO);
        return Result.success(setmealVOPageResult);
    }
    /**
     * 套餐起售和停售
     */
    @PostMapping("/status/{status}")
    public Result startOrEnd(@PathVariable Integer status,Long id){
        log.info("套餐状态：{},套餐id:{}",status,id);
        setmealService.startOrEnd(status,id);

        return Result.success();
    }
    /**
     * 根据id查询套餐信息
     */
    @GetMapping("/{id}")
    public Result<SetmealVO> getById(@PathVariable Long id){
        log.info("根据id查询套餐信息：{}",id);
        SetmealVO setmealVO = setmealService.getById(id);
        return Result.success(setmealVO);
    }
    /**
     * 编辑套餐信息
     */
    @PutMapping()
    public Result updateSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("编辑套餐信息：{}",setmealDTO);
        setmealService.updateSetmeal(setmealDTO);
        return Result.success();
    }

    /**
    * 根据id删除套餐
    */
    @DeleteMapping
    public Result deleteSetmeal(@RequestParam List<Long> ids){
    log.info("根据id删除套餐：{}",ids);
    setmealService.removeByIds(ids);
    return Result.success();
    }



    /**
     * 套餐查询
     */
    @GetMapping("/list")
    public Result<List<SetmealVO>> list(){
        log.info("套餐查询"); return null;
    }
}
