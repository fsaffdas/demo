package com.sky.exception;

public class DatabaseInsertExceptions extends BaseException{
    public DatabaseInsertExceptions() {
        super();
    }

    public DatabaseInsertExceptions(String msg) {
        super(msg);
    }
}
