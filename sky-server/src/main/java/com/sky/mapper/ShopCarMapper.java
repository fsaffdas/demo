package com.sky.mapper;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ShopCarMapper {
    List<ShoppingCart> list(ShoppingCart shoppingCart);

    @Update("update shopping_cart set number=#{number} where id=#{id}")
    void update(ShoppingCart cart);

    @Insert("insert into shopping_cart(name, image, user_id, dish_id, setmeal_id, dish_flavor, " +
            "amount, create_time) " +
            "VALUE(#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{amount},#{createTime}) ")
    void insert(ShoppingCart shoppingCart);

    @Select("select * from shopping_cart")
    List<ShoppingCart> getAll();

    @Delete("delete from shopping_cart where id=#{id}")
    void delete(ShoppingCart shoppingCart);


    @Delete("delete from shopping_cart where id=#{id}")
    void deleteById(Long id);
}
