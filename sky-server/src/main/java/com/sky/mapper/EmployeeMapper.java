package com.sky.mapper;

import com.sky.entity.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from employee where username = #{username}")
    Employee getByUsername(String username);

    /**
     * 根据idnumber查询用户信息
     * @param idNumber
     */
    @Select("select * from employee where id_number=#{idNumber} ")
    Integer getByUserIdNubmer(String idNumber);

    /**
     * 添加员工信息
     * @param employee
     * @return
     */
    @Insert("insert into employee(name, username, password, phone, sex, id_number," +
            " create_time, update_time, create_user, update_user) " +
            "VALUE (#{name},#{username},#{password},#{phone},#{sex},#{idNumber}," +
            "#{createTime},#{updateTime},#{createUser},#{updateUser})")
    Integer insert(Employee employee);

    List<Employee> pageQuery(int pageNumber, int pageSize, String name);

    @Select("select * from employee")
    List<Employee> queryAll();

    void updateById(Employee employee);
    @Select("select * from employee where id=#{id}")
    Employee getByUserId(Long id);
}
