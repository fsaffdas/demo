package com.sky.service.impl;

import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.exception.CategoryNameOccupy;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class CategotyServiceImpl implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;


    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    @Override
    public PageResult pageQuery(CategoryPageQueryDTO categoryPageQueryDTO) {
        int page = categoryPageQueryDTO.getPage();
        int pageSize = categoryPageQueryDTO.getPageSize();
        String name = categoryPageQueryDTO.getName();
        Integer type = categoryPageQueryDTO.getType();
        List<Category> categoryList=categoryMapper.pageQuery((page-1)*pageSize,pageSize,name,type);
        Long total=categoryMapper.queryAll();
        return new PageResult(total,categoryList);
    }
    /**添加菜品信息
     *
     */
    @Override
    public void add(CategoryDTO categoryDTO) {
        if (categoryMapper.getByName(categoryDTO.getName())!=null){
            throw new CategoryNameOccupy(MessageConstant.Category_Name_Occupy);
        }
        Category category=new Category();
        BeanUtils.copyProperties(categoryDTO,category);
        category.setStatus(0);
        category.setCreateTime(LocalDateTime.now());
        category.setCreateUser(BaseContext.getCurrentId());
        category.setUpdateTime(LocalDateTime.now());
        category.setUpdateUser(BaseContext.getCurrentId());
        categoryMapper.insert(category);
    }
    /**
     * 根据id更新状态
     */
    @Override
    public void updateStatusById(Long id, Integer status) {
        categoryMapper.updateStatusById(id,status);
    }
    /**
     * 编辑菜品信息
     */
    @Override
    public void edit(CategoryDTO categoryDTO) {
        Category category=new Category();
        BeanUtils.copyProperties(categoryDTO,category);

        category.setUpdateUser(BaseContext.getCurrentId());
        category.setUpdateTime(LocalDateTime.now());

        categoryMapper.update(category);

    }

    @Override
    public void deleteById(Long id) {
        Integer integer = dishMapper.selectCountByCategoryId(id);
        if (integer>0){
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        Integer result = setmealMapper.selectCountByCategoryId(id);
        if (result>0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        categoryMapper.dete(id);
    }

    @Override
    public List<Category> list(Integer type) {
        return categoryMapper.list(type);
    }
}
