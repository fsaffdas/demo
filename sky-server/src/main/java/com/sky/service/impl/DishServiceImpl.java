package com.sky.service.impl;

import com.fasterxml.jackson.databind.ser.Serializers;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.exception.CategoryNameOccupy;
import com.sky.exception.DeletionNotAllowedException;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;

    /**
     * 添加菜品
     * @param dishDTO
     */
    @Override
    @Transactional
    public void addDish(DishDTO dishDTO) {
        if (dishMapper.getByName(dishDTO.getName())!=null){
            throw new CategoryNameOccupy("菜品名字已存在");
        }
        Dish dish=new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        List<DishFlavor> flavors = dishDTO.getFlavors();

        dish.setCreateUser(BaseContext.getCurrentId());
        dish.setCreateTime(LocalDateTime.now());
        dish.setUpdateTime(LocalDateTime.now());
        dish.setUpdateUser(BaseContext.getCurrentId());


        dishMapper.addDish(dish);
        //获取insert语句生成的主键值
        Long dishId = dish.getId();
        if (flavors!=null&&flavors.size()>0){
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishId);
            });
            dishFlavorMapper.addfalvors(flavors);
        }

    }

    /**
     * 分页查询
     */
    @Override
    public PageResult pageDish(DishPageQueryDTO dishPageQueryDTO) {
        int page = dishPageQueryDTO.getPage();
        int pageSize = dishPageQueryDTO.getPageSize();
        Integer status = dishPageQueryDTO.getStatus();
        Integer categoryId = dishPageQueryDTO.getCategoryId();
        String name = dishPageQueryDTO.getName();

        List<DishVO> list=dishMapper.pageDish((page-1)*pageSize,pageSize,status,categoryId,name);
        Long total=dishMapper.queryCount();
        return new PageResult(total,list);
    }
    /**
     *更改菜品出售状态
     */
    @Override
    public void openOrClose(Long id, Integer status) {
        dishMapper.updateStatus(id,status);
    }
    /**
     * 编辑菜品信息
     */
    @Override
    @Transactional
    public void edit(DishDTO dishDTO) {

        Dish dish=new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dish.setUpdateUser(BaseContext.getCurrentId());
        dish.setUpdateTime(LocalDateTime.now());
        dishMapper.updateDish(dish);
        List<DishFlavor> flavors = dishDTO.getFlavors();

        dishFlavorMapper.deleteById(dish.getId());
        if (flavors!=null&&flavors.size()>0){
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dish.getId());
            });
        dishFlavorMapper.addfalvors(flavors);

    }}
    /**
     * 通过id获取菜品信息
     */
    @Override
    public DishVO getById(Long id) {
        DishVO dishVO=new DishVO();
        Dish dish = dishMapper.getById(id);
        BeanUtils.copyProperties(dish,dishVO);

        List<DishFlavor> list  =dishFlavorMapper.getFlavorBYDishId(id);
        dishVO.setFlavors(list);
        return dishVO;
    }
    /**
     * 删除菜品信息
     */
    @Override
    @Transactional
    public void deleteDish(List<Long> ids) {
        //起售的菜品不能删除
        List<Dish> dishList=dishMapper.getDishByIds(ids);
        dishList.forEach(dish -> {
            if (dish.getStatus()==1){
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        });
        //关联套餐的不能删除
        List<Long> idList=setmealDishMapper.getByDishId(ids);
        if (idList!=null&&idList.size()>0){
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        //执行删除，删除dish表中信息，dishflavor表中信息
        dishMapper.deleteByIds(ids);
        dishFlavorMapper.deleteByIds(ids);
    }
    /**
     * 根据菜品分类id查询菜品
     *
     */
    @Override
    public List<DishVO> getDishByCategoryId(Long categoryId) {
        List<Dish> dishList=dishMapper.getDishByCategoryId(categoryId);
        List<DishVO> dishVOList=new ArrayList<>();
        dishList.forEach(dish -> {
            DishVO dishVO=new DishVO();
            BeanUtils.copyProperties(dish,dishVO);
            dishVOList.add(dishVO);
        });
        return dishVOList;
    }
    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    public List<DishVO> listWithFlavor(Dish dish) {
        List<Dish> dishList = dishMapper.getDishByCategoryId(dish.getCategoryId());

        List<DishVO> dishVOList = new ArrayList<>();

        for (Dish d : dishList) {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d,dishVO);

            //根据菜品id查询对应的口味
            List<DishFlavor> flavors = dishFlavorMapper.getFlavorBYDishId(d.getId());

            dishVO.setFlavors(flavors);
            dishVOList.add(dishVO);
        }

        return dishVOList;
    }
}
