package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.utils.MinioClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.UUID;

@RestController
@Slf4j
@RequestMapping("/admin/common")
public class UpLoadController {
    @Autowired
    private MinioClientUtil minioClientUtil;

    @PostMapping("/upload")
    public Result<String> upload(MultipartFile file){
        log.info("上传的文件:{}",file);

        String url = minioClientUtil.upload(file);
        log.info("图片路径:{}",url);
        return Result.success(url);
    }
}
