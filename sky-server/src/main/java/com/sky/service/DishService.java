package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {

    void addDish(DishDTO dishDTO);

    PageResult pageDish(DishPageQueryDTO dishPageQueryDTO);

    void openOrClose(Long id, Integer status);

    void edit(DishDTO dishDTO);

    DishVO getById(Long id);

    void deleteDish(List<Long> ids);

    List<DishVO> getDishByCategoryId(Long categoryId);

    List<DishVO> listWithFlavor(Dish dish);
}
