package com.sky.service.impl;

import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.SetmealEnableFailedException;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;

    /**
     * 添加套餐
     * @param setmealDTO
     */
    @Override
    @Transactional
    public void addSetmeal(SetmealDTO setmealDTO) {
        //向setmeal表中添加套餐信息
        Setmeal setmeal=new Setmeal();
        String name = setmealDTO.getName();
        if (setmealMapper.getByName(name)!= null){
            throw new SetmealEnableFailedException("套餐名已存在");
        }
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmeal.setCreateUser(BaseContext.getCurrentId());
        setmeal.setCreateTime(LocalDateTime.now());
        setmeal.setUpdateUser(BaseContext.getCurrentId());
        setmeal.setUpdateTime(LocalDateTime.now());
        log.info("套餐信息："+setmeal);
        setmealMapper.addSetmeal(setmeal);
        log.info("套餐id:{}",setmeal.getId());
        //向setmeal_dish表中添加套餐菜品信息
        SetmealDish newSetmealDish=new SetmealDish();
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        List<SetmealDish> newSetmealDishes = new ArrayList<>();
        setmealDishes.forEach(setmealDish ->{
            BeanUtils.copyProperties(setmealDish,newSetmealDish);
            newSetmealDish.setSetmealId(setmeal.getId());
            newSetmealDishes.add(newSetmealDish);
        });
        setmealDishMapper.addSetmealDishes(newSetmealDishes);
    }
    /**
     * 套餐分类查询
     */
    @Override
    public PageResult page(SetmealPageQueryDTO setmealPageQueryDTO) {
        int page = setmealPageQueryDTO.getPage();
        int pageSize = setmealPageQueryDTO.getPageSize();
        int start = (page-1)*pageSize;
        String name = setmealPageQueryDTO.getName();
        Integer status = setmealPageQueryDTO.getStatus();
        Integer categoryId = setmealPageQueryDTO.getCategoryId();
        List<SetmealVO> list=setmealMapper.page(start,pageSize,name,status,categoryId);
        Integer total = setmealMapper.count();
        return new PageResult(total,list);
    }

    /**
     * 起售和停售套餐
     * @param status
     * @param id
     */
    @Override
    public void startOrEnd(Integer status, Long id) {

        setmealMapper.updateStatus(status,id);
    }
    /**
     * 根据id查询套餐信息
     */
    @Override
    @Transactional
    public SetmealVO getById(Long id) {
        SetmealVO setmealVO=new SetmealVO();
        Setmeal setmeal= setmealMapper.getById(id);
        BeanUtils.copyProperties(setmeal,setmealVO);
        List<SetmealDish> setmealDishes=setmealDishMapper.getBySetmealId(id);
        setmealVO.setSetmealDishes(setmealDishes);
        return setmealVO;
    }
    /**
     * 编辑套餐信息
     */
    @Override
    @Transactional
    public void updateSetmeal(SetmealDTO setmealDTO) {
        Setmeal setmeal=new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmeal.setUpdateTime(LocalDateTime.now());
        setmeal.setUpdateUser(BaseContext.getCurrentId());
        setmealMapper.update(setmeal);

        setmealDishMapper.deleteBySetmealId(setmealDTO.getId());
        SetmealDish setmealDish=new SetmealDish();
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        List<SetmealDish> newSetmealDishes = new ArrayList<>();
        for (SetmealDish dish : setmealDishes) {
            BeanUtils.copyProperties(dish,setmealDish);
            setmealDish.setSetmealId(setmealDTO.getId());
            newSetmealDishes.add(setmealDish);
        }
        setmealDishMapper.addSetmealDishes(newSetmealDishes);

    }

    /**
     * 根据id删除套餐
     * @param ids
     */
    @Override
    @Transactional
    public void removeByIds(List<Long> ids) {
        if (ids.size()==0){
            throw new SetmealEnableFailedException("套餐不存在");
        }
        //套餐是否存在，不存在报异常
        List<Setmeal> idsList=setmealMapper.getByIds(ids);
        if (idsList.size()==0){
            throw new SetmealEnableFailedException("套餐不存在");
        }
        log.info("idsList:{}",idsList);
        //起售中的套餐不能删除
        for (Setmeal setmeal : idsList) {
            if (setmeal.getStatus()==1){
                throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ON_SALE);
            }
        }
        setmealMapper.deleteByIds(ids);
        //删除套餐菜品
        setmealDishMapper.deleteBySetmealIds(ids);

    }

    @Override
    public List<Setmeal> list(Setmeal setmeal) {
        List<Setmeal> list = setmealMapper.list(setmeal);
        return list;
    }

    @Override
    public List<DishItemVO> getDishItemById(Long id) {
        return setmealMapper.getDishItemBySetmealId(id);
    }

}
