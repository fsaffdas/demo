package com.sky.exception;

public class CategoryNameOccupy extends BaseException{
    public CategoryNameOccupy() {
        super();
    }

    public CategoryNameOccupy(String msg) {
        super(msg);
    }
}
