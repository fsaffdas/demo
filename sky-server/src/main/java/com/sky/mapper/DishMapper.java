package com.sky.mapper;

import com.sky.entity.Dish;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public  interface  DishMapper {

    /**
     * 根据分类id查询菜品数量
     * @param categoryId
     * @return
     */
    @Select("select count(id) from dish where category_id=#{categoryId}")
    Integer selectCountByCategoryId(Long categoryId);

    void addDish(Dish dish);
    @Select("select * from dish where name=#{name}")
    Dish getByName(String name);

    List<DishVO> pageDish(int page, int pageSize, Integer status, Integer categoryId, String name);

    /**
     * 查询菜品条数
     * @return
     */
    @Select("select count(id) from dish")
    Long queryCount();

    @Update("update dish set status=#{status} where id=#{id}")
    void updateStatus(Long id, Integer status);

    @Select("select * from dish where  id=#{id}")
    Dish getById(Long id);


    List<Dish> getDishByIds(List<Long> ids);

    void deleteByIds(List<Long> ids);


    void updateDish(Dish dish);

    @Select("select * from dish where category_id=#{categoryId}")
    List<Dish> getDishByCategoryId(Long categoryId);
}
