package com.sky.mapper;

import com.sky.entity.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryMapper {

    List<Category> pageQuery(int page, int pageSize, String name, Integer type);

    @Select("select count(*) from category")
    Long queryAll();
@Select("select * from category where name=#{name}")
    Category getByName(String name);
    @Insert("insert into category(type, name, sort, status, create_time, " +
            "update_time, create_user, update_user)" +
            " VALUE (#{type},#{name},#{sort},#{status},#{createTime},#{updateTime},#{createUser}" +
            ",#{updateUser})")
    void insert(Category category);

    @Update("update category set status=#{status} where id=#{id}")
    void updateStatusById(Long id, Integer status);

    void update(Category category);

    @Delete("delete from category where id=#{id}")
    void dete(Long id);

    List<Category> list(Integer type);
}
