package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/dish")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;

    /**
     *
     * 添加菜品信息
     * @param dishDTO
     * @return
     */
    @PostMapping
    public Result addDish(@RequestBody DishDTO dishDTO){
      log.info("添加的菜品信息:{}",dishDTO);
      dishService.addDish(dishDTO);
      return Result.success();
    }

    /**
     * 分页查询
     * @param dishPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> pageDish( DishPageQueryDTO dishPageQueryDTO){
            log.info("分页信息:{}",dishPageQueryDTO);
            PageResult pageResult= dishService.pageDish(dishPageQueryDTO);
            return Result.success(pageResult);

    }
    /**
     * 修改状态
     */
    @PostMapping("/status/{status}")
    public Result openOrClose( Long id,@PathVariable Integer status){
        log.info("状态信息:{},id信息:{}",status,id);
        dishService.openOrClose(id,status);
        return Result.success();
    }
    /**
     * 根据id获取菜品信息
     */
    @GetMapping("/{id}")
    public Result<DishVO> getById(@PathVariable Long id){
        log.info("要修改菜品的id:{}",id);
        DishVO dish= dishService.getById(id);
        return  Result.success(dish);
    }
    /**
     * 编辑菜品信息
     */
    @PutMapping
    public Result edit(@RequestBody DishDTO dishDTO){
        log.info("修改后的菜品信息:{}",dishDTO);
        dishService.edit(dishDTO);
        return Result.success();
    }
    /**
     * 删除菜品信息
     */
    @DeleteMapping()
    public Result delete(@RequestParam List<Long> ids) {
        log.info("要删除的id为:{}",ids);

        dishService.deleteDish(ids);
        return Result.success();
    }
    /**
     * 根据分类id查询菜品
     */
    @GetMapping("list")
    public Result<List<DishVO>> getDishByCategotyId(Long categoryId){
        log.info("要查询的分类id:{}",categoryId);
        List<DishVO> dishVOS= dishService.getDishByCategoryId(categoryId);
        return Result.success(dishVOS);

    }

}
