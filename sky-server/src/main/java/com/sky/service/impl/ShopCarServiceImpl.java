package com.sky.service.impl;

import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.ShoppingCart;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShopCarMapper;
import com.sky.service.ShopCartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ShopCarServiceImpl implements ShopCartService {
    @Autowired
    private ShopCarMapper shopCarMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;

    @Override
    public void add(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart=new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);
        //从当前线程里获取用户id
        Long id = BaseContext.getCurrentId();
        shoppingCart.setUserId(id);
        //判断购物车是否已存在商品
        List<ShoppingCart> list= shopCarMapper.list(shoppingCart);
        //如果存在数量加一
        if (list!=null&&list.size()>0){
            ShoppingCart cart = list.get(0);
            cart.setNumber(cart.getNumber()+1);
            //跟新购物车
            shopCarMapper.update(cart);
        }else{
            //不存在创建购物车
            //判断添加的是菜品还是套餐
            Long dishId = shoppingCartDTO.getDishId();
            if (dishId!=null){
                //通过dishid查询出dish将菜品名称，图片，价格赋值给购物车
                Dish dish = dishMapper.getById(dishId);
                shoppingCart.setName(dish.getName());
                shoppingCart.setImage(dish.getImage());
                shoppingCart.setAmount(dish.getPrice());

            }else {
                //将套餐信息添加到购物车
                //根据id获取套餐信息
                Setmeal setmeal = setmealMapper.getById(shoppingCart.getSetmealId());
                shoppingCart.setName(setmeal.getName());
                shoppingCart.setImage(setmeal.getImage());
                shoppingCart.setAmount(setmeal.getPrice());
            }
            //将shopingcart完善
            //设置创建时间
            shoppingCart.setCreateTime(LocalDateTime.now());
            //设置商品数量
            shoppingCart.setNumber(1);
            //添加购物车
            shopCarMapper.insert(shoppingCart);
        }


    }

    /**
     * 查看购物车
     * @return
     */
    @Override
    public List<ShoppingCart> list() {
        return shopCarMapper.getAll();
    }

    @Override
    public void sub(ShoppingCartDTO shoppingCartDTO) {
        ShoppingCart shoppingCart=new ShoppingCart();
        BeanUtils.copyProperties(shoppingCartDTO,shoppingCart);
        shoppingCart.setUserId(BaseContext.getCurrentId());
        //获取原理的数量
        List<ShoppingCart> list = shopCarMapper.list(shoppingCart);
        if (list!=null&&list.size()>0){
            ShoppingCart shoppingCart1 = list.get(0);
            Integer number = shoppingCart1.getNumber();
            shoppingCart.setId(shoppingCart1.getId());
            if (number>1){
                //当数量大于1时数量减一
                shoppingCart.setNumber(number-1);
                shopCarMapper.update(shoppingCart);
            }else {
                //当数量等于1时删除数据
                shopCarMapper.delete(shoppingCart);
            }

        }
    }

    @Override
    public void clear() {
        Long id = BaseContext.getCurrentId();
        shopCarMapper.deleteById(id);
    }
}
