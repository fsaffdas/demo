package com.sky.exception;

public class AccountAlreadyExist extends BaseException{
    public AccountAlreadyExist() {
        super();
    }

    public AccountAlreadyExist(String msg) {
        super(msg);
    }
}
